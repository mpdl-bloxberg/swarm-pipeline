# Install

add to repo with: 
```
git submodule add https://gitlab.mpcdf.mpg.de/mpdl-bloxberg/swarm-pipeline.git
cp swarm-pipeline/.gitlab-ci-swarm.yml .
```

update with: 
```
cd swarm-pipeline
git checkout main 
git pull 
cd ..
cp swarm-pipeline/.gitlab-ci-swarm.yml . 
```
